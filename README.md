![mail-x](/uploads/7513fdb94f7ff9625eaf75f17a552975/mail-x.png)
# Notification API

RESTful API for managing and configuring notifications that allows integration by any other platform.  

![Java](https://img.shields.io/badge/java-007396.svg?style=for-the-badge&logo=oracle&logoColor=white)
![Spring](https://img.shields.io/badge/spring-%236DB33F.svg?style=for-the-badge&logo=spring&logoColor=white)
![Maven](https://img.shields.io/badge/maven-862474.svg?style=for-the-badge&logo=apache&logoColor=white)
![GitLab](https://img.shields.io/badge/gitlab-%23fb8500.svg?style=for-the-badge&logo=gitlab&logoColor=white)
![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
![Heroku](https://img.shields.io/badge/heroku-79589F.svg?style=for-the-badge&logo=heroku&logoColor=white)

> _Status:_ **🚧  Notification API 🚀 Development  🚧**  
> _Current version API endpoints:_ **v0**

## Tools and technology
The following technologies and frameworks were used to develop the project:  
* [Java](https://docs.oracle.com/javase/7/docs/technotes/guides/language/)
* [Spring Boot](https://spring.io/)  
* [Spring Data JPA](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/)  
* [Spring Security](https://docs.spring.io/spring-security/reference/index.html)  
* [Maven](https://maven.apache.org/index.html)
* [H2 Database](https://www.h2database.com/html/main.html)
* [GitLab](https://about.gitlab.com/)
* [Docker](https://docs.docker.com/)
* [Heroku](https://www.heroku.com/home)

## Demo
See the working demo of the Notification API deployed on the Heroku Cloud Application Platform. For that visit this link [https://jsouza-notification-api.herokuapp.com/v0/swagger-ui.html](https://jsouza-notification-api.herokuapp.com/v0/swagger-ui.html) and use the swagger provided by the API.

## Features
### Implemented features
The API provides the following functionality through its endpoints:  
* Login  
    `POST /login HTTP:1.1`
* Create new user  
    `POST /users/register HTTP/1.1`
* Search for a user  
    `GET /users/{id} HTTP/1.1`
* Create new app  
    `POST /apps HTTP/1.1`
* Retrieve app data  
    `GET /apps/{id} HTTP/1.1`
* Enable/disable WebPush Channel  
    `PUT /apps/{app_id}/webpushes/settings HTTP/1.1`
* Configure WebPush  
    `POST /apps/{app_id}/webpushes/settings HTTP/1.1`
* Retrieve WebPush settings  
    `GET /apps/{app_id}/webpushes/settings HTTP/1.1`
* Send WebPush notification  
    `POST /apps/{app_id}/webpushes/notification HTTP/1.1`
* Recover WebPush notification history  
    `GET /apps/{app_id}/webpushes/notifications?initdate={initdate}&enddate={enddate} HTTP/1.1`
* Retrieve WebPush notification details  
    `GET /apps/{app_id}/webpush/notifications/{notification_id} HTTP/1.1`

### Future features
These features will be developed soon (any help is welcome ❤️):
* Enable/disable Email Channel
* Configure Email
* Retrieve Email settings
* Send Email notification  
* Enable/disable SMS Channel
* Configure SMS
* Retrieve SMS settings
* Send SMS notification 

## Getting started
Before starting, you will need to have the following tools installed on your machine:
* [Java™ Development Kit (JDK) 17+](https://www.oracle.com/java/technologies/downloads/)
* [Git 2.25+](https://git-scm.com)
* [Apache Maven 3.8.6+](https://maven.apache.org/download.cgi)
    
Also it's nice to have an editor to work with the code like  [VSCode](https://code.visualstudio.com/).

## Quick start
```
# Clone this repository
$ git clone https://gitlab.com/josemar.souza/notification-api.git

# Access project folder in terminal/cmd
$ cd notification-api

# Clean and install
$ ./mvnw clean install

# Run the API 
$ ./mvnw spring-boot:run

# The server will start on port:8080 
# Access http://localhost:8080/v0/swagger-ui.html to consult the endpoints documentation.
```
## Run with docker image
You need to have Docker installed on your machine. Access this tutorial to learn how to install docker [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)
```
# Pull the image from DockerHub
$ docker pull jmsouzadev/notification-api

# Run the container
$ docker run -p 8080:8080 notification-api

# The server will start on port:8080 
# Access [http://localhost:8080/v0/swagger-ui.html](http://localhost:8080/v0/swagger-ui.html) to consult the endpoints documentation.
```

## Endpoints documentation
The detailed documentation of each of the endpoints can be consulted in the OpenAPI standard (Swagger 2.0). To do so, after running the application, access the url [https://jsouza-notification-api.herokuapp.com/v0/swagger-ui.html](https://jsouza-notification-api.herokuapp.com/v0/swagger-ui.html).

## Next steps
To improve the project and complete all future features, you will need:
* Expand test coverage
* Document the code