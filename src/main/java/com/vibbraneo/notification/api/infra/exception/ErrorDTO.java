package com.vibbraneo.notification.api.infra.exception;

public record ErrorDTO(String error) {
    public ErrorDTO(Exception ex) {
        this(ex.getMessage());
    }
}
