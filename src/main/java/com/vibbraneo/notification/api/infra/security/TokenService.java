package com.vibbraneo.notification.api.infra.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.vibbraneo.notification.api.domain.user.User;
import com.vibbraneo.notification.api.domain.user.UserRepository;
import com.vibbraneo.notification.api.infra.exception.CustomException;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

@Service
public class TokenService {

    @Autowired
    private UserRepository userRepository;

    @Value("${api.security.token.secret}")
    private String secret;

    @Value("${api.security.token.issuer}")
    private String issuer;

    @Value("${api.security.token.invalid.message}")
    private String invalidTokenMessage;

    public String generateToken(User user) {
        try {
            var algorithm = Algorithm.HMAC256(secret);
            return JWT.create().withIssuer(this.issuer).withSubject(user.getUsername()).withExpiresAt(dueDate())
                    .sign(algorithm);
        } catch (JWTCreationException exception) {
            throw new CustomException("Failed to generate jwt token");
        }
    }

    public String getSubject(String tokenJWT) {
        try {
            var algorithm = Algorithm.HMAC256(secret);
            return JWT.require(algorithm).withIssuer(this.issuer).build().verify(tokenJWT).getSubject();
        } catch (JWTVerificationException exception) {
            throw new AccessDeniedException(invalidTokenMessage);
        }
    }

    public User getUserFromTokenJWT(String tokenJWT) {
        String email = this.getSubject(tokenJWT);
        Optional<User> user = userRepository.findByEmail(email);
        if (!user.isPresent()) {
            throw new AccessDeniedException(invalidTokenMessage);
        }
        return user.get();
    }

    public String getTokenFromRequest(HttpServletRequest request) {
        try {
            return request.getHeader("Authorization").split(" ")[1];
        } catch (Exception e) {
            throw new AccessDeniedException(invalidTokenMessage);
        }
    }

    private Instant dueDate() {
        return LocalDateTime.now().plusHours(2).toInstant(ZoneOffset.of("-05:00"));
    }

}
