package com.vibbraneo.notification.api.infra.exception;

/**
 * Custom class for Exceptions
 * 
 * @author josemar.souza
 * @since 2002-02-20
 * @version 0.1
 */
public class CustomException extends RuntimeException {
    public CustomException(String message) {
        super(message);
    }
}