package com.vibbraneo.notification.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.vibbraneo.notification.api.domain.app.App;
import com.vibbraneo.notification.api.domain.app.AppService;
import com.vibbraneo.notification.api.domain.app.dto.AppDTO;
import com.vibbraneo.notification.api.domain.app.dto.CreatedAppDTO;
import com.vibbraneo.notification.api.domain.app.dto.NewAppDTO;
import com.vibbraneo.notification.api.infra.exception.ErrorDTO;
import com.vibbraneo.notification.api.infra.security.TokenService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/apps")
@SecurityRequirement(name = "bearer-key")
public class AppController {

    @Autowired
    private AppService service;

    @Autowired
    private TokenService tokenService;

    @GetMapping("/{id}")
    @Operation(summary = "Returns the details of an App", description = "To get the details, please enter the App Id.", tags = {
            "Apps" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
            @ApiResponse(responseCode = "400", description = "Error when performing the Get.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
    public ResponseEntity<AppDTO> getUserById(@PathVariable Long id, HttpServletRequest request) {
        String tokenJWT = tokenService.getTokenFromRequest(request);
        App app = service.getAppByIdAndToken(id, tokenJWT);
        return ResponseEntity.ok(new AppDTO(app));
    }

    @PostMapping
    @Transactional
    @Operation(summary = "Create a App", description = "To generate a new app, it is necessary to make a post in the /apps resource, informing the necessary data to create the application.", tags = {
            "Apps" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
            @ApiResponse(responseCode = "400", description = "Error when performing.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
    public ResponseEntity<CreatedAppDTO> createApp(@RequestBody @Valid NewAppDTO dto, UriComponentsBuilder uriBuilder,
            HttpServletRequest request) {
        String tokenJWT = tokenService.getTokenFromRequest(request);
        CreatedAppDTO appCreatedDTO = this.service.createApp(dto.app_name(), tokenJWT);
        var uri = uriBuilder.path("/apps/{id}").buildAndExpand(appCreatedDTO.app_id()).toUri();
        return ResponseEntity.created(uri).body(appCreatedDTO);
    }
}
