package com.vibbraneo.notification.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import jakarta.validation.Valid;

import com.vibbraneo.notification.api.domain.user.UserService;
import com.vibbraneo.notification.api.domain.user.dto.NewUserDTO;
import com.vibbraneo.notification.api.domain.user.dto.CreatedUserDTO;
import com.vibbraneo.notification.api.domain.user.dto.UserDTO;
import com.vibbraneo.notification.api.infra.exception.ErrorDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/{id}")
    @SecurityRequirement(name = "bearer-key")
    @Operation(summary = "Returns the details of an User", description = "To get the details, please enter the User Id.", tags = {
            "Users" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
            @ApiResponse(responseCode = "400", description = "Error when performing the Get.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {
        UserDTO userDTO = service.getUserById(id);
        return ResponseEntity.ok(userDTO);
    }

    @PostMapping("/register")
    @Transactional
    @Operation(summary = "Register a new User", description = "To register a new user, it is necessary to make a post in the /apps resource, informing the necessary data to create the user.", tags = {
            "Users" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
            @ApiResponse(responseCode = "400", description = "Error when performing.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
    public ResponseEntity<CreatedUserDTO> createUser(@RequestBody @Valid NewUserDTO dto,
            UriComponentsBuilder uriBuilder) {
        CreatedUserDTO userCreatedDTO = this.service.createUser(dto);
        var uri = uriBuilder.path("/users/{id}").buildAndExpand(userCreatedDTO.id()).toUri();
        return ResponseEntity.created(uri).body(userCreatedDTO);
    }
}