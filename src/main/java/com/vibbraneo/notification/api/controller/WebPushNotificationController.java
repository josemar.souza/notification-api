package com.vibbraneo.notification.api.controller;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.vibbraneo.notification.api.domain.notification.Notification;
import com.vibbraneo.notification.api.domain.notification.dto.CreatedNotificationDTO;
import com.vibbraneo.notification.api.domain.notification.dto.NotificationHistoryDTO;
import com.vibbraneo.notification.api.domain.notification.dto.NotificationsDTO;
import com.vibbraneo.notification.api.domain.notification.webpush.WebPushNotification;
import com.vibbraneo.notification.api.domain.notification.webpush.WebPushNotificationService;
import com.vibbraneo.notification.api.domain.notification.webpush.dto.NewWebPushDTO;
import com.vibbraneo.notification.api.domain.notification.webpush.dto.WebPushNotificationDTO;
import com.vibbraneo.notification.api.infra.exception.ErrorDTO;
import com.vibbraneo.notification.api.infra.security.TokenService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/apps/{app_id}")
@SecurityRequirement(name = "bearer-key")
public class WebPushNotificationController {

        @Autowired
        private WebPushNotificationService webPushNotificationService;

        @Autowired
        private TokenService tokenService;

        @PostMapping("/webpushes/notification")
        @Transactional
        @Operation(summary = "Sends a notification through the WebPush channel", description = "To send a notification through the WebPush channel, please enter the App Id.", tags = {
                        "Notifications" })
        @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
                        @ApiResponse(responseCode = "400", description = "Error when performing.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
        public ResponseEntity<CreatedNotificationDTO> sendWebPushNotification(
                        @RequestBody @Valid NewWebPushDTO newWebPushDTO, @PathVariable("app_id") Long appId,
                        UriComponentsBuilder uriBuilder, HttpServletRequest request) {
                String tokenJWT = tokenService.getTokenFromRequest(request);
                CreatedNotificationDTO createdNotificationDTO = webPushNotificationService.send(appId, newWebPushDTO,
                                tokenJWT);
                var uri = uriBuilder.path("/apps/{app_id}/webpushes/notifications/{notification_id}")
                                .buildAndExpand(createdNotificationDTO.notification_id(), appId).toUri();
                return ResponseEntity.created(uri).body(createdNotificationDTO);
        }

        @GetMapping("/webpushes/notifications")
        @Operation(summary = "Returns a simple list of notifications sent by the WebPush channel", description = "To get a simple list of notifications sent by the WebPush channel, please enter the App Id and date interval.", tags = {
                        "Notifications" })
        @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
                        @ApiResponse(responseCode = "400", description = "Error when performing the Get.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
        public ResponseEntity<NotificationsDTO> getWebPushNotifications(@PathVariable("app_id") Long appId,
                        @RequestParam @DateTimeFormat(pattern = "yyyyMMdd") LocalDate initdate,
                        @RequestParam @DateTimeFormat(pattern = "yyyyMMdd") LocalDate enddate,
                        HttpServletRequest request) {
                String tokenJWT = tokenService.getTokenFromRequest(request);
                Collection<Notification> notifications = webPushNotificationService.findAllByChannelAndInterval(appId,
                                initdate, enddate, tokenJWT);
                return ResponseEntity.ok(new NotificationsDTO(
                                notifications.stream().map(NotificationHistoryDTO::new).collect(Collectors.toList())));
        }

        @GetMapping("/webpushes/notifications/{notification_id}")
        @Operation(summary = "Returns the details of a WebPush Notification", description = "To get the details of the details of a WebPush Notification, please enter the App Id and the Notification Id.", tags = {
                        "Notifications" })
        @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
                        @ApiResponse(responseCode = "400", description = "Error when performing the Get.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
        public ResponseEntity<WebPushNotificationDTO> sgetWebPushNotifications(@PathVariable("app_id") Long appId,
                        @PathVariable("notification_id") Long notificationId, HttpServletRequest request) {
                String tokenJWT = tokenService.getTokenFromRequest(request);
                WebPushNotification notification = (WebPushNotification) webPushNotificationService.get(appId,
                                notificationId, tokenJWT);
                return ResponseEntity.ok(new WebPushNotificationDTO(notification));
        }
}
