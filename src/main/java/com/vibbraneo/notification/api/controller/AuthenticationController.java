package com.vibbraneo.notification.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vibbraneo.notification.api.domain.user.UserRepository;
import com.vibbraneo.notification.api.domain.user.dto.LoginDTO;
import com.vibbraneo.notification.api.domain.user.dto.LoginSuccessDTO;
import com.vibbraneo.notification.api.domain.user.dto.UserDTO;
import com.vibbraneo.notification.api.infra.exception.ErrorDTO;
import com.vibbraneo.notification.api.infra.security.TokenService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/login")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager manager;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserRepository userRepository;

    @PostMapping
    @Operation(summary = "Perform user authentication", description = "Authenticates the user and returns the TokenJWT.", tags = {
            "Users" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
            @ApiResponse(responseCode = "400", description = "Error when performing the Get.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
    public ResponseEntity<LoginSuccessDTO> doLogin(@RequestBody @Valid LoginDTO dto) {
        var authenticationToken = new UsernamePasswordAuthenticationToken(dto.login(), dto.password());
        manager.authenticate(authenticationToken);
        var user = userRepository.findByEmail(dto.login());
        if (!user.isPresent()) {
            throw new EntityNotFoundException();
        }
        var tokenJWT = tokenService.generateToken(user.get());
        return ResponseEntity.ok(new LoginSuccessDTO(tokenJWT, new UserDTO(user.get())));
    }
}