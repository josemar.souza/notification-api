package com.vibbraneo.notification.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vibbraneo.notification.api.domain.channel.dto.StatusChannelDTO;
import com.vibbraneo.notification.api.domain.channel.webpush.WebPushService;
import com.vibbraneo.notification.api.domain.channel.webpush.dto.WebPushSettingsDTO;
import com.vibbraneo.notification.api.infra.exception.ErrorDTO;
import com.vibbraneo.notification.api.infra.security.TokenService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/apps/{app_id}/webpushes/settings")
@SecurityRequirement(name = "bearer-key")
public class WebPushChannelController {

        @Autowired
        private WebPushService service;

        @Autowired
        private TokenService tokenService;

        @GetMapping
        @Operation(summary = "Returns the configuration details of the WebPush Channel", description = "To get the details of WebPush channel, please enter the App Id.", tags = {
                        "Channels" })
        @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
                        @ApiResponse(responseCode = "400", description = "Error when performing the Get.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
        public ResponseEntity<WebPushSettingsDTO> getSettings(@PathVariable("app_id") Long appId,
                        HttpServletRequest request) {
                String tokenJWT = tokenService.getTokenFromRequest(request);
                var webPushSettingsDTO = this.service.getSettings(appId, tokenJWT);
                return ResponseEntity.ok(webPushSettingsDTO);
        }

        @PutMapping
        @Transactional
        @Operation(summary = "Change status of WebPush channel", description = "To change the status of WebPush channel, please enter the App Id.", tags = {
                        "Channels" })
        @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
                        @ApiResponse(responseCode = "400", description = "Error when performing.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
        public ResponseEntity<StatusChannelDTO> changeStatus(@PathVariable("app_id") Long appId,
                        HttpServletRequest request) {
                String tokenJWT = tokenService.getTokenFromRequest(request);
                StatusChannelDTO statusChannelDTO = service.changeStatus(appId, tokenJWT);
                return ResponseEntity.ok(statusChannelDTO);
        }

        @PostMapping
        @ResponseStatus(HttpStatus.OK)
        @Operation(summary = "Performs the configuration of the WebPush channel", description = "To configure WebPush channel, please enter the App Id.", tags = {
                        "Channels" })
        @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Successfully performed operation."),
                        @ApiResponse(responseCode = "400", description = "Error when performing.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "403", description = "Not allowed.", content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
                        @ApiResponse(responseCode = "404", description = "Resource not found.", content = @Content()) })
        @Transactional
        public void doConfigure(@PathVariable("app_id") Long appId, @RequestBody @Valid WebPushSettingsDTO dto,
                        HttpServletRequest request) {
                String tokenJWT = tokenService.getTokenFromRequest(request);
                service.doConfigure(appId, tokenJWT, dto);
        }
}
