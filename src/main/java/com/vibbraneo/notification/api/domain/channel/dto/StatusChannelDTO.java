package com.vibbraneo.notification.api.domain.channel.dto;

public record StatusChannelDTO(Integer previous_status, Integer current_status) {
    public StatusChannelDTO(Boolean currentStatus) {
        this(Boolean.FALSE.equals(currentStatus) ? 1 : 0, Boolean.TRUE.equals(currentStatus) ? 1 : 0);
    }
}