package com.vibbraneo.notification.api.domain.notification;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vibbraneo.notification.api.domain.channel.Channel;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
    @Query("""
            SELECT n FROM Notification n
            WHERE
            n.channel = :channel AND
            n.sendDate >= :initDate AND
            n.sendDate <= :endDate
                """)
    Collection<Notification> findAllByChannelAndInterval(Channel channel, LocalDate initDate, LocalDate endDate);

    Optional<Notification> findByIdAndChannel(Long id, Channel channel);

}