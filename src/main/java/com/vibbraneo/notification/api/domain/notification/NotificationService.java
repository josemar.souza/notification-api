package com.vibbraneo.notification.api.domain.notification;

import java.time.LocalDate;
import java.util.Collection;

import com.vibbraneo.notification.api.domain.notification.dto.CreatedNotificationDTO;

public interface NotificationService {
    CreatedNotificationDTO send(Long appId, Object notificationDTO, String tokenJWT);

    Notification get(Long appId, Long notificationId, String tokenJWT);

    Collection<Notification> findAllByChannelAndInterval(Long appId, LocalDate initdate, LocalDate enddate,
            String tokenJWT);
}
