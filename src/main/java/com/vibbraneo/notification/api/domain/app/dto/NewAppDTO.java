package com.vibbraneo.notification.api.domain.app.dto;

import jakarta.validation.constraints.NotBlank;

public record NewAppDTO(@NotBlank(message = "App name is required.") String app_name) {

}
