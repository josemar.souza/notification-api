package com.vibbraneo.notification.api.domain.channel;

import com.vibbraneo.notification.api.domain.app.App;
import com.vibbraneo.notification.api.domain.channel.Channel;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "channels", uniqueConstraints = { @UniqueConstraint(columnNames = { "app_id", "name" }) })
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = { "id", "app", "name" })
public class Channel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "app_id")
    private App app;

    @Enumerated(EnumType.STRING)
    private ChannelType name;

    private Boolean active = false;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "setting_id")
    private ChannelSetting setting;

    public Channel(App app, ChannelType name, Boolean active) {
        this.app = app;
        this.name = name;
        this.active = active;
    }

    public void changeStatus() {
        this.active = !this.active;
    }

    public void updateSetting(ChannelSetting setting) {
        this.setting = setting;
    }

}
