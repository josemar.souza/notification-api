package com.vibbraneo.notification.api.domain.notification.validations;

import com.vibbraneo.notification.api.domain.notification.Notification;

public interface NotificationValidation {
    void validate(Notification notification);
}
