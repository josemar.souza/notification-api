package com.vibbraneo.notification.api.domain.channel.webpush.dto;

import com.vibbraneo.notification.api.domain.channel.webpush.WebPushSetting;

public record WebPushSettingsDTO(SettingWebPushDTO settings) {
    public WebPushSettingsDTO(WebPushSetting webPushSetting) {
        this(new SettingWebPushDTO(webPushSetting));
    }
}
