package com.vibbraneo.notification.api.domain.channel.webpush.dto;

import com.vibbraneo.notification.api.domain.channel.webpush.WelcomeNotification;

public record WelcomeNotificationDTO(String message_title, String message_text, Integer enable_url_redirect,
        String url_redirect) {
    public WelcomeNotificationDTO(WelcomeNotification welcomeNotification) {
        this(welcomeNotification.getMessageTitle(), welcomeNotification.getMessageText(),
                welcomeNotification.getEnableUrlRedirect(), welcomeNotification.getUrlRedirect());
    }
}
