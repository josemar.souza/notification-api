package com.vibbraneo.notification.api.domain.channel.webpush;

import com.vibbraneo.notification.api.domain.channel.webpush.dto.SiteDTO;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import jakarta.persistence.Table;

@Entity
@Table(name = "sites")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Site {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    private String urlIcon;

    @OneToOne(mappedBy = "site")
    private WebPushSetting webPushSetting;

    public Site(SiteDTO site) {
        this.name = site.name();
        this.address = site.address();
        this.urlIcon = site.url_icon();
    }

    public void update(SiteDTO site) {
        this.name = site.name();
        this.address = site.address();
        this.urlIcon = site.url_icon();
    }
}
