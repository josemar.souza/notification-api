package com.vibbraneo.notification.api.domain.channel.webpush;

import com.vibbraneo.notification.api.domain.channel.webpush.dto.AllowNotificationDTO;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "allow_notifications")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class AllowNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String messageText;
    private String allowButtonText;
    private String denyButtonText;

    @OneToOne(mappedBy = "allowNotification")
    private WebPushSetting webPushSetting;

    public AllowNotification(AllowNotificationDTO allowNotification) {
        this.messageText = allowNotification.message_text();
        this.allowButtonText = allowNotification.allow_button_text();
        this.denyButtonText = allowNotification.deny_button_text();
    }

    public void update(AllowNotificationDTO allowNotification) {
        this.messageText = allowNotification.message_text();
        this.allowButtonText = allowNotification.allow_button_text();
        this.denyButtonText = allowNotification.deny_button_text();
    }
}
