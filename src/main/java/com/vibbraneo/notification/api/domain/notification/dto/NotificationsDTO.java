package com.vibbraneo.notification.api.domain.notification.dto;

import java.util.List;

public record NotificationsDTO(List<NotificationHistoryDTO> notifications) {
}
