package com.vibbraneo.notification.api.domain.notification.webpush.dto;

import java.time.LocalDate;
import java.util.Set;

import com.vibbraneo.notification.api.domain.notification.webpush.WebPushNotification;

public record WebPushNotificationDTO(Long notification_id, Set<String> audience_segments, String message_title,
		String message_text, String icon_url, String redirect_url, LocalDate send_date, Long received, Long opened,
		Long app_id) {
	public WebPushNotificationDTO(WebPushNotification webPushNotification) {
		this(webPushNotification.getId(), webPushNotification.getAudienceSegments(),
				webPushNotification.getMessageTitle(), webPushNotification.getMessageText(),
				webPushNotification.getIconUrl(), webPushNotification.getRedirectUrl(),
				webPushNotification.getSendDate(), Boolean.TRUE.equals(webPushNotification.getReceived()) ? 1L : 0L,
				Boolean.TRUE.equals(webPushNotification.getOpened()) ? 1L : 0L,
				webPushNotification.getChannel().getApp().getId());
	}
}
