package com.vibbraneo.notification.api.domain.channel.webpush;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vibbraneo.notification.api.domain.app.AppService;
import com.vibbraneo.notification.api.domain.channel.Channel;
import com.vibbraneo.notification.api.domain.channel.ChannelService;
import com.vibbraneo.notification.api.domain.channel.ChannelType;
import com.vibbraneo.notification.api.domain.channel.dto.StatusChannelDTO;
import com.vibbraneo.notification.api.domain.channel.webpush.dto.WebPushSettingsDTO;

import jakarta.persistence.EntityNotFoundException;

@Service
public class WebPushService implements ChannelService {

    @Autowired
    private AppService appService;

    private static final ChannelType channelType = ChannelType.WEB_PUSH;

    @Override
    public StatusChannelDTO changeStatus(Long appId, String tokenJWT) {
        Boolean currentStatus = appService.changeStatusChannel(appId, WebPushService.channelType, tokenJWT);
        return new StatusChannelDTO(currentStatus);
    }

    @Override
    public void doConfigure(Long appId, String tokenJWT, Object dto) {
        var webPushSettingsDTO = (WebPushSettingsDTO) dto;
        var app = this.appService.getAppByIdAndToken(appId, tokenJWT);

        Optional<Channel> channel = app.getChannels().stream().filter(c -> c.getName().isWebPush()).findAny();

        if (!channel.isPresent()) {
            throw new EntityNotFoundException("Channel not exist.");
        }

        if (channel.get().getSetting() != null) {
            WebPushSetting webPushSetting = (WebPushSetting) channel.get().getSetting();
            webPushSetting.update(webPushSettingsDTO);
        } else {
            WebPushSetting webPushSettingNew = new WebPushSetting(webPushSettingsDTO, channel.get());
            channel.get().updateSetting(webPushSettingNew);
        }
        this.appService.save(app);
    }

    public WebPushSettingsDTO getSettings(Long appId, String tokenJWT) {
        var app = this.appService.getAppByIdAndToken(appId, tokenJWT);
        Optional<Channel> channel = app.getChannels().stream().filter(c -> c.getName().isWebPush()).findAny();

        if (!channel.isPresent()) {
            throw new EntityNotFoundException("Channel not exist.");
        }

        if (channel.get().getSetting() == null) {
            throw new EntityNotFoundException("Channel not configured.");
        }

        return new WebPushSettingsDTO((WebPushSetting) channel.get().getSetting());

    }

}
