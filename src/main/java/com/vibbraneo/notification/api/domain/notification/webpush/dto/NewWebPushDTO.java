package com.vibbraneo.notification.api.domain.notification.webpush.dto;

import java.util.Set;
import java.util.HashSet;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

public record NewWebPushDTO(
		@NotEmpty(message = "Enter at least one in audience_segments") Set<String> audience_segments,
		@NotBlank(message = "message_title is required") String message_title,
		@NotBlank(message = "message_text is required") String message_text,
		@NotBlank(message = "icon_url is required") String icon_url,
		@NotBlank(message = "redirect_url is required") String redirect_url) {
	public NewWebPushDTO() {
		this(new HashSet<>(), "", "", "", "");
	}
}
