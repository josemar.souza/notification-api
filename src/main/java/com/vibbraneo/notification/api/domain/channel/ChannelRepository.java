package com.vibbraneo.notification.api.domain.channel;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vibbraneo.notification.api.domain.app.App;

public interface ChannelRepository extends JpaRepository<Channel, Long> {
    Channel findByAppAndName(App app, ChannelType channelType);
}
