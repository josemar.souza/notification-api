package com.vibbraneo.notification.api.domain.channel.webpush;

import com.vibbraneo.notification.api.domain.channel.Channel;
import com.vibbraneo.notification.api.domain.channel.ChannelSetting;
import com.vibbraneo.notification.api.domain.channel.webpush.dto.WebPushSettingsDTO;

import jakarta.persistence.CascadeType;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@DiscriminatorValue("WebPushSetting")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WebPushSetting extends ChannelSetting {

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "site_id")
    private Site site;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "allowNotification_id")
    private AllowNotification allowNotification;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "welcomeNotification_id")
    private WelcomeNotification welcomeNotification;

    public WebPushSetting(WebPushSettingsDTO dto, Channel channel) {
        this.site = new Site(dto.settings().site());
        this.allowNotification = new AllowNotification(dto.settings().allow_notification());
        this.welcomeNotification = new WelcomeNotification(dto.settings().welcome_notification());
        super.setChannel(channel);
    }

    public void update(WebPushSettingsDTO dto) {
        this.site.update(dto.settings().site());
        this.allowNotification.update(dto.settings().allow_notification());
        this.welcomeNotification.update(dto.settings().welcome_notification());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((site == null) ? 0 : site.hashCode());
        result = prime * result + ((allowNotification == null) ? 0 : allowNotification.hashCode());
        result = prime * result + ((welcomeNotification == null) ? 0 : welcomeNotification.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WebPushSetting other = (WebPushSetting) obj;
        if (site == null) {
            if (other.site != null)
                return false;
        } else if (!site.equals(other.site))
            return false;
        if (allowNotification == null) {
            if (other.allowNotification != null)
                return false;
        } else if (!allowNotification.equals(other.allowNotification))
            return false;
        if (welcomeNotification == null) {
            if (other.welcomeNotification != null)
                return false;
        } else if (!welcomeNotification.equals(other.welcomeNotification))
            return false;
        return true;
    }

}
