package com.vibbraneo.notification.api.domain.notification.webpush;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vibbraneo.notification.api.domain.app.AppService;
import com.vibbraneo.notification.api.domain.channel.ChannelRepository;
import com.vibbraneo.notification.api.domain.channel.ChannelType;
import com.vibbraneo.notification.api.domain.notification.Notification;
import com.vibbraneo.notification.api.domain.notification.NotificationRepository;
import com.vibbraneo.notification.api.domain.notification.NotificationService;
import com.vibbraneo.notification.api.domain.notification.dto.CreatedNotificationDTO;
import com.vibbraneo.notification.api.domain.notification.validations.FindHistoryValidation;
import com.vibbraneo.notification.api.domain.notification.validations.NotificationValidation;
import com.vibbraneo.notification.api.domain.notification.webpush.dto.NewWebPushDTO;

import jakarta.persistence.EntityNotFoundException;

@Service
public class WebPushNotificationService implements NotificationService {
    @Autowired
    private AppService appService;
    
    @Autowired
    private ChannelRepository channelRepository;

    @Autowired
    private NotificationRepository repository;

    @Autowired
    private List<NotificationValidation> notificationValidations;

    @Autowired
    private List<FindHistoryValidation> findHistoryValidations;

    private static final ChannelType channelType = ChannelType.WEB_PUSH;

    @Override
    public CreatedNotificationDTO send(Long appId, Object dto, String tokenJWT) {
        NewWebPushDTO newWebPushDTO = (NewWebPushDTO)dto;
        var app = appService.getAppByIdAndToken(appId, tokenJWT);
        var channel = channelRepository.findByAppAndName(app, channelType);
        WebPushNotification notification = new WebPushNotification(channel, newWebPushDTO);

        //Validate all conditions
        notificationValidations.forEach(v -> v.validate(notification));

        repository.save(notification);
        return new CreatedNotificationDTO(notification);
    }

    @Override
    public Collection<Notification> findAllByChannelAndInterval(Long appId, LocalDate initDate,
            LocalDate endDate, String tokenJWT) {
        //Validate all conditions
        findHistoryValidations.forEach(v -> v.validate(initDate, endDate));

        var app = appService.getAppByIdAndToken(appId, tokenJWT);
        var channel = channelRepository.findByAppAndName(app, channelType);
        return repository.findAllByChannelAndInterval(channel, initDate, endDate);
    }

    @Override
    public Notification get(Long appId, Long notificationId, String tokenJWT) {
        var app = appService.getAppByIdAndToken(appId, tokenJWT);
        var channel = channelRepository.findByAppAndName(app, channelType);
        var notification = repository.findByIdAndChannel(notificationId, channel);
        if(!notification.isPresent()) {
            throw new EntityNotFoundException(
                String.format(
                    "Notification id %s does not exist or does not belong to the authenticated user.", 
                    notificationId.toString()
                )
            );
        }

        return notification.get();
    }
}
