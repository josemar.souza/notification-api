package com.vibbraneo.notification.api.domain.channel;

import com.vibbraneo.notification.api.domain.channel.dto.StatusChannelDTO;

public interface ChannelService {
    StatusChannelDTO changeStatus(Long appId, String tokenJWT);

    void doConfigure(Long appId, String tokenJWT, Object dto);
}
