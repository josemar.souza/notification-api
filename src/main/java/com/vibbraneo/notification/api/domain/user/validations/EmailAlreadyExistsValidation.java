package com.vibbraneo.notification.api.domain.user.validations;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vibbraneo.notification.api.domain.user.User;
import com.vibbraneo.notification.api.domain.user.UserRepository;
import com.vibbraneo.notification.api.infra.exception.CustomException;

@Component
public class EmailAlreadyExistsValidation implements NewUserValidation {

    @Autowired
    private UserRepository repository;

    @Override
    public void validate(User user) {
        Optional<User> userExists = repository.findByEmail(user.getEmail());
        if(userExists.isPresent()){
            throw new CustomException(String.format("Email %s already exists.", user.getEmail()));
        }        
    }
    
}
