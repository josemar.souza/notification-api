package com.vibbraneo.notification.api.domain.channel.webpush;

import com.vibbraneo.notification.api.domain.channel.webpush.dto.WelcomeNotificationDTO;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import jakarta.persistence.Table;

@Entity
@Table(name = "welcome_notifications")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class WelcomeNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String messageTitle;
    private String messageText;
    private Integer enableUrlRedirect;
    private String urlRedirect;

    @OneToOne(mappedBy = "welcomeNotification")
    private WebPushSetting webPushSetting;    

    public WelcomeNotification(WelcomeNotificationDTO dto) {
        this.messageText = dto.message_text();
        this.messageTitle = dto.message_title();
        this.enableUrlRedirect = dto.enable_url_redirect();
        this.urlRedirect = dto.url_redirect();
    }

    public void update(WelcomeNotificationDTO dto) {
        this.messageText = dto.message_text();
        this.messageTitle = dto.message_title();
        this.enableUrlRedirect = dto.enable_url_redirect();
        this.urlRedirect = dto.url_redirect();
    }
}
