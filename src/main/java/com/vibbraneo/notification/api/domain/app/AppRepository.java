package com.vibbraneo.notification.api.domain.app;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vibbraneo.notification.api.domain.user.User;

public interface AppRepository extends JpaRepository<App, Long> {
    Optional<App> findByIdAndUser(Long id, User user);
}
