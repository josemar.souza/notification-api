package com.vibbraneo.notification.api.domain.channel.webpush.dto;

import com.vibbraneo.notification.api.domain.channel.webpush.WebPushSetting;

public record SettingWebPushDTO(SiteDTO site, AllowNotificationDTO allow_notification,
        WelcomeNotificationDTO welcome_notification) {
    public SettingWebPushDTO(WebPushSetting webPushSetting) {
        this(new SiteDTO(webPushSetting.getSite()), new AllowNotificationDTO(webPushSetting.getAllowNotification()),
                new WelcomeNotificationDTO(webPushSetting.getWelcomeNotification()));
    }
}
