package com.vibbraneo.notification.api.domain.user.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record NewUserDTO(
        @Email(message = "Incorrect or invalid email") @NotBlank(message = "email is required") String email,

        @NotBlank(message = "password is required") String password,

        @NotBlank(message = "name is required") String name,

        @NotBlank(message = "company_name is required") String company_name,

        String phone_number, String company_address) {

}
