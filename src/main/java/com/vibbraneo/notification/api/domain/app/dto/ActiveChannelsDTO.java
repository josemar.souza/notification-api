package com.vibbraneo.notification.api.domain.app.dto;

import com.vibbraneo.notification.api.domain.app.App;

public record ActiveChannelsDTO(Boolean webpush, Boolean email, Boolean sms) {
    public ActiveChannelsDTO(App app) {
        this(app.isWebpushChannelActive(), app.isEmailChannelActive(), app.isSmsChannelActive());
    }
}
