package com.vibbraneo.notification.api.domain.notification.dto;

import com.vibbraneo.notification.api.domain.notification.webpush.WebPushNotification;

public record CreatedNotificationDTO(Long notification_id) {
    public CreatedNotificationDTO(WebPushNotification notification) {
        this(notification.getId());
    }
}
