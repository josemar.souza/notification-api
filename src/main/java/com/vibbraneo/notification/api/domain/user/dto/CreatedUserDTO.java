package com.vibbraneo.notification.api.domain.user.dto;

import com.vibbraneo.notification.api.domain.user.User;

public record CreatedUserDTO(Long id) {
    public CreatedUserDTO(User user) {
        this(user.getId());
    }
}
