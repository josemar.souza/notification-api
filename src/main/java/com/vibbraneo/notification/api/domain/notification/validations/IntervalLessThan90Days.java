package com.vibbraneo.notification.api.domain.notification.validations;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.vibbraneo.notification.api.infra.exception.CustomException;

@Component
public class IntervalLessThan90Days implements FindHistoryValidation {
    @Override
    public void validate(LocalDate initDate, LocalDate endDate) {
        if (initDate.plusDays(90L).compareTo(endDate) < 0) {
            throw new CustomException("Interval greater than 90 days.");
        }
    }

}
