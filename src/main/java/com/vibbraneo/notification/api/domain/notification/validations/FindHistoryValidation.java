package com.vibbraneo.notification.api.domain.notification.validations;

import java.time.LocalDate;

public interface FindHistoryValidation {
    void validate(LocalDate initDate, LocalDate endDate);
}
