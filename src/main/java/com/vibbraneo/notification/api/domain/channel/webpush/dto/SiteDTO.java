package com.vibbraneo.notification.api.domain.channel.webpush.dto;

import com.vibbraneo.notification.api.domain.channel.webpush.Site;

public record SiteDTO(String name, String address, String url_icon) {
    public SiteDTO(Site site) {
        this(site.getName(), site.getAddress(), site.getUrlIcon());
    }
}
