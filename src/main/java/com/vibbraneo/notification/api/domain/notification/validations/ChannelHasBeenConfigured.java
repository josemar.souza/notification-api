package com.vibbraneo.notification.api.domain.notification.validations;

import org.springframework.stereotype.Component;

import com.vibbraneo.notification.api.domain.notification.Notification;
import com.vibbraneo.notification.api.infra.exception.CustomException;

@Component
public class ChannelHasBeenConfigured implements NotificationValidation {

    @Override
    public void validate(Notification notification) {
        if (notification.getChannel().getSetting() == null) {
            throw new CustomException("Channel not configured.");
        }
    }

}
