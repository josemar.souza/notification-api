package com.vibbraneo.notification.api.domain.notification.validations;

import org.springframework.stereotype.Component;

import com.vibbraneo.notification.api.domain.notification.Notification;
import com.vibbraneo.notification.api.infra.exception.CustomException;

@Component
public class ChannelIsEnabled implements NotificationValidation {

    @Override
    public void validate(Notification notification) {
        if (Boolean.FALSE.equals(notification.getChannel().getActive())) {
            throw new CustomException("Channel is not enabled.");
        }
    }

}
