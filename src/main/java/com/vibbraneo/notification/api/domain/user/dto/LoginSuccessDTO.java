package com.vibbraneo.notification.api.domain.user.dto;

public record LoginSuccessDTO(String token, AuthenticatedUserDTO user) {
    public LoginSuccessDTO(String token, UserDTO userDTO) {
        this(token, new AuthenticatedUserDTO(userDTO));
    }
}
