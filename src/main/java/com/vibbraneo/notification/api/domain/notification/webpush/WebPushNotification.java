package com.vibbraneo.notification.api.domain.notification.webpush;

import java.time.LocalDate;
import java.util.Set;

import com.vibbraneo.notification.api.domain.channel.Channel;
import com.vibbraneo.notification.api.domain.notification.Notification;
import com.vibbraneo.notification.api.domain.notification.webpush.dto.NewWebPushDTO;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@DiscriminatorValue("WebPushNotification")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WebPushNotification extends Notification {

    @ElementCollection
    private Set<String> audienceSegments;

    private String iconUrl;

    private String redirectUrl;

    private Boolean opened;

    public WebPushNotification(Channel channel, NewWebPushDTO dto) {
        super.setChannel(channel);
        super.setMessageTitle(dto.message_title());
        super.setMessageText(dto.message_text());
        super.setSendDate(LocalDate.now());
        super.setReceived(false);
        this.opened = false;
        this.audienceSegments = dto.audience_segments();
        this.iconUrl = dto.icon_url();
        this.redirectUrl = dto.redirect_url();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((audienceSegments == null) ? 0 : audienceSegments.hashCode());
        result = prime * result + ((iconUrl == null) ? 0 : iconUrl.hashCode());
        result = prime * result + ((redirectUrl == null) ? 0 : redirectUrl.hashCode());
        result = prime * result + ((opened == null) ? 0 : opened.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WebPushNotification other = (WebPushNotification) obj;
        if (audienceSegments == null) {
            if (other.audienceSegments != null)
                return false;
        } else if (!audienceSegments.equals(other.audienceSegments))
            return false;
        if (iconUrl == null) {
            if (other.iconUrl != null)
                return false;
        } else if (!iconUrl.equals(other.iconUrl))
            return false;
        if (redirectUrl == null) {
            if (other.redirectUrl != null)
                return false;
        } else if (!redirectUrl.equals(other.redirectUrl))
            return false;
        if (opened == null) {
            if (other.opened != null)
                return false;
        } else if (!opened.equals(other.opened))
            return false;
        return true;
    }
}
