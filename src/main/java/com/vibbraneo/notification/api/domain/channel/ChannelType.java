package com.vibbraneo.notification.api.domain.channel;

public enum ChannelType {
    SMS("SMS"), EMAIL("E-mail"), WEB_PUSH("Web Push");

    private final String description;

    private ChannelType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public boolean isEmail() {
        return this.equals(ChannelType.EMAIL);
    }

    public boolean isWebPush() {
        return this.equals(ChannelType.WEB_PUSH);
    }

    public boolean isSMS() {
        return this.equals(ChannelType.SMS);
    }
}
