package com.vibbraneo.notification.api.domain.app;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vibbraneo.notification.api.domain.app.dto.CreatedAppDTO;
import com.vibbraneo.notification.api.domain.app.dto.AppDTO;
import com.vibbraneo.notification.api.domain.channel.Channel;
import com.vibbraneo.notification.api.domain.channel.ChannelType;
import com.vibbraneo.notification.api.infra.security.TokenService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class AppService {

    @Autowired
    private AppRepository repository;

    @Autowired
    private TokenService tokenService;

    public CreatedAppDTO createApp(String name, String tokenJWT) {
        var user = this.tokenService.getUserFromTokenJWT(tokenJWT);
        var token = String.valueOf(name.concat(user.getUsername()).hashCode());
        var app = new App(name, user, token);
        var channelTypes = Arrays.asList(ChannelType.values());
        for (ChannelType channelType : channelTypes) {
            app.addChannel(new Channel(app, channelType, false));
        }
        this.repository.save(app);
        return new CreatedAppDTO(app);
    }

    public App getAppByIdAndToken(Long id, String tokenJWT) {
        var user = this.tokenService.getUserFromTokenJWT(tokenJWT);
        var app = this.repository.findByIdAndUser(id, user);
        if (!app.isPresent()) {
            throw new EntityNotFoundException(String
                    .format("App id %s does not exist or does not belong to the authenticated user.", id.toString()));
        }
        return app.get();
    }

    public AppDTO getAppDTOById(Long id, String tokenJWT) {
        var app = this.getAppByIdAndToken(id, tokenJWT);
        return new AppDTO(app);
    }

    public Boolean changeStatusChannel(Long appId, ChannelType channelType, String tokenJWT) {
        var currentStatus = false;
        var app = this.getAppByIdAndToken(appId, tokenJWT);
        for (var channel : app.getChannels()) {
            if (channel.getName().equals(channelType)) {
                channel.changeStatus();
                currentStatus = channel.getActive();
            }
        }

        return currentStatus;
    }

    public App save(App app) {
        return this.repository.save(app);
    }
}
