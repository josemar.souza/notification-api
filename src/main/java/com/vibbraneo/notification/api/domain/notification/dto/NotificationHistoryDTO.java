package com.vibbraneo.notification.api.domain.notification.dto;

import com.vibbraneo.notification.api.domain.notification.Notification;

import java.time.LocalDate;

public record NotificationHistoryDTO(Long notification_id, LocalDate send_date) {
    public NotificationHistoryDTO(Notification notification) {
        this(notification.getId(), notification.getSendDate());
    }
}
