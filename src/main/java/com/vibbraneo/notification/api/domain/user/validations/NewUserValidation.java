package com.vibbraneo.notification.api.domain.user.validations;

import com.vibbraneo.notification.api.domain.user.User;

public interface NewUserValidation {
    void validate(User user);
}
