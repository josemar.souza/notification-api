package com.vibbraneo.notification.api.domain.app;

import java.util.HashSet;
import java.util.Set;

import com.vibbraneo.notification.api.domain.channel.Channel;
import com.vibbraneo.notification.api.domain.user.User;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "apps", uniqueConstraints = { @UniqueConstraint(columnNames = { "user_id", "name" }) })
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class App {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String token;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "app", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Channel> channels = new HashSet<>();

    public App(String name, User user, String token) {
        this.name = name;
        this.user = user;
        this.token = token;
    }

    public void addChannel(Channel channel) {
        this.channels.add(channel);
    }

    public boolean isWebpushChannelActive() {
        return this.channels.stream().anyMatch(c -> c.getName().isWebPush() && c.getActive());
    }

    public boolean isEmailChannelActive() {
        return this.channels.stream().anyMatch(c -> c.getName().isEmail() && c.getActive());
    }

    public boolean isSmsChannelActive() {
        return this.channels.stream().anyMatch(c -> c.getName().isSMS() && c.getActive());
    }
}
