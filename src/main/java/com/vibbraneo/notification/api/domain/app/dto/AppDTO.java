package com.vibbraneo.notification.api.domain.app.dto;

import com.vibbraneo.notification.api.domain.app.App;

public record AppDTO(Long app_id, String app_name, String app_token, ActiveChannelsDTO active_channels) {
	public AppDTO(App app) {
		this(app.getId(), app.getName(), app.getToken(), new ActiveChannelsDTO(app));
	}
}
