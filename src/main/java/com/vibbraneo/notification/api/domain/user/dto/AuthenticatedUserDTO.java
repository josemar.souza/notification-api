package com.vibbraneo.notification.api.domain.user.dto;

public record AuthenticatedUserDTO(Long id, String name, String email) {
    public AuthenticatedUserDTO(UserDTO userDTO) {
        this(userDTO.id(), userDTO.name(), userDTO.email());
    }
}
