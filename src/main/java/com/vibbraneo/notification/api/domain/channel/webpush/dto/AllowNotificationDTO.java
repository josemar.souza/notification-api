package com.vibbraneo.notification.api.domain.channel.webpush.dto;

import com.vibbraneo.notification.api.domain.channel.webpush.AllowNotification;

public record AllowNotificationDTO(String message_text, String allow_button_text, String deny_button_text) {
    public AllowNotificationDTO(AllowNotification allowNotification) {
        this(allowNotification.getMessageText(), allowNotification.getAllowButtonText(),
                allowNotification.getDenyButtonText());
    }
}
