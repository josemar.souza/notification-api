package com.vibbraneo.notification.api.domain.notification;

import java.time.LocalDate;

import com.vibbraneo.notification.api.domain.channel.Channel;

import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "channel_id")
    private Channel channel;

    private String messageTitle;

    private String messageText;

    private LocalDate sendDate;

    private Boolean received = false;

    protected void setChannel(Channel channel) {
        this.channel = channel;
    }

    protected void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    protected void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    protected void setSendDate(LocalDate sendDate) {
        this.sendDate = sendDate;
    }

    protected void setReceived(Boolean received) {
        this.received = received;
    }

}
