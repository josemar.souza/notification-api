package com.vibbraneo.notification.api.domain.user;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.vibbraneo.notification.api.domain.user.dto.NewUserDTO;
import com.vibbraneo.notification.api.domain.user.dto.CreatedUserDTO;
import com.vibbraneo.notification.api.domain.user.dto.UserDTO;
import com.vibbraneo.notification.api.domain.user.validations.NewUserValidation;
import com.vibbraneo.notification.api.infra.security.SecurityConfigurations;

import jakarta.persistence.EntityNotFoundException;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private SecurityConfigurations securityConfigurations;

    @Autowired
    private List<NewUserValidation> createUserValidations;

    public UserDTO getUserById(Long id) {
        Optional<User> user = repository.findById(id);
        if (!user.isPresent()) {
            throw new EntityNotFoundException(String.format("User id %s does not exist.", id.toString()));
        }
        return new UserDTO(user.get());
    }

    public CreatedUserDTO createUser(NewUserDTO dto) {
        var user = new User(dto);
        user.setEncryptedPassword(this.encryptPassword(dto.password()));

        // Validate all conditions
        createUserValidations.forEach(v -> v.validate(user));

        repository.save(user);

        return new CreatedUserDTO(user);
    }

    private String encryptPassword(String plainPassword) {
        PasswordEncoder passwordEncoder = this.securityConfigurations.passwordEncoder();
        return passwordEncoder.encode(plainPassword);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = repository.findByEmail(username);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("Username (e-mail) not found!");
        }

        return user.get();
    }
}
