package com.vibbraneo.notification.api.domain.app.dto;

import com.vibbraneo.notification.api.domain.app.App;

public record CreatedAppDTO(Long app_id, String app_token) {
    public CreatedAppDTO(App app) {
        this(app.getId(), app.getToken());
    }
}
