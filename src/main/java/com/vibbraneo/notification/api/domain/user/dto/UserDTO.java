package com.vibbraneo.notification.api.domain.user.dto;

import com.vibbraneo.notification.api.domain.user.User;

public record UserDTO(Long id, String email, String name, String company_name, String phone_number,
        String company_address) {
    public UserDTO(User user) {
        this(user.getId(), user.getEmail(), user.getName(), user.getCompanyName(), user.getPhoneNumber(),
                user.getCompanyAddress());
    }
}
