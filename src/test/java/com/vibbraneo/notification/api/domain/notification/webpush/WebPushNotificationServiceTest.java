package com.vibbraneo.notification.api.domain.notification.webpush;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.vibbraneo.notification.api.domain.app.App;
import com.vibbraneo.notification.api.domain.app.AppRepository;
import com.vibbraneo.notification.api.domain.channel.Channel;
import com.vibbraneo.notification.api.domain.channel.ChannelType;
import com.vibbraneo.notification.api.domain.channel.webpush.WebPushService;
import com.vibbraneo.notification.api.domain.channel.webpush.WebPushSetting;
import com.vibbraneo.notification.api.domain.channel.webpush.dto.AllowNotificationDTO;
import com.vibbraneo.notification.api.domain.channel.webpush.dto.SettingWebPushDTO;
import com.vibbraneo.notification.api.domain.channel.webpush.dto.SiteDTO;
import com.vibbraneo.notification.api.domain.channel.webpush.dto.WebPushSettingsDTO;
import com.vibbraneo.notification.api.domain.channel.webpush.dto.WelcomeNotificationDTO;
import com.vibbraneo.notification.api.domain.notification.webpush.dto.NewWebPushDTO;
import com.vibbraneo.notification.api.domain.user.User;
import com.vibbraneo.notification.api.domain.user.UserRepository;
import com.vibbraneo.notification.api.domain.user.dto.NewUserDTO;
import com.vibbraneo.notification.api.infra.security.TokenService;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class WebPushNotificationServiceTest {

    @Autowired
    private WebPushNotificationService service;

    @Autowired
    private WebPushService webpushService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AppRepository appRepository;

    @Autowired
    private UserRepository userRepository;

    private final ChannelType channelType = ChannelType.WEB_PUSH;

    @Test
    @DisplayName("Should throws CustomException when channel is not configured")
    void testSendWebPushNotificationCase1() {
        User userTest = getUserTest("testcase1@mail.com");
        App appTest = getAppTestNotSettingAndEnabledChannel(userTest, "My App 1");

        String tokenJWT = tokenService.generateToken(userTest);

        // Enable the channel
        webpushService.changeStatus(appTest.getId(), tokenJWT);

        var dto = new NewWebPushDTO(new HashSet<String>(Arrays.asList("Teste")), "", "", "", "");

        Exception exception = assertThrows(Exception.class, () -> {
            service.send(appTest.getId(), dto, tokenJWT);
        });

        assertEquals("Channel not configured.", exception.getMessage());

    }

    @Test
    @DisplayName("Should throws CustomException when channel is not enabled")
    void testSendWebPushNotificationCase2() {
        User userTest = getUserTest("testcase2@mail.com");
        App appTest = getAppTestDisabledChannelAndConfigured(userTest, "My App 2");
        String tokenJWT = tokenService.generateToken(userTest);

        var dto = new NewWebPushDTO(new HashSet<String>(Arrays.asList("Teste")), "", "", "", "");

        Exception exception = assertThrows(Exception.class, () -> {
            service.send(appTest.getId(), dto, tokenJWT);
        });

        assertEquals("Channel is not enabled.", exception.getMessage());
    }

    private App getAppTestNotSettingAndEnabledChannel(User user, String appName) {
        var app = new App(appName, user, LocalDateTime.now().toInstant(ZoneOffset.UTC).toString());
        app.addChannel(new Channel(app, channelType, true));
        return appRepository.save(app);
    }

    private App getAppTestDisabledChannelAndConfigured(User user, String appName) {
        var app = new App(appName, user, LocalDateTime.now().toInstant(ZoneOffset.UTC).toString());
        var channel = new Channel(app, channelType, false);
        var settingsDTO = new WebPushSettingsDTO(new SettingWebPushDTO(new SiteDTO("", "", ""),
                new AllowNotificationDTO("", "", ""), new WelcomeNotificationDTO("", "", 0, "")));
        channel.updateSetting(new WebPushSetting(settingsDTO, channel));
        app.addChannel(channel);
        return appRepository.save(app);
    }

    private User getUserTest(String email) {
        return userRepository.saveAndFlush(new User(new NewUserDTO(email, "13213", "User Test", "", "", "")));
    }

}
